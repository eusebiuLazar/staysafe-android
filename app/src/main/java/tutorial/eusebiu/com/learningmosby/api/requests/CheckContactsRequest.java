package tutorial.eusebiu.com.learningmosby.api.requests;

import java.util.List;

import tutorial.eusebiu.com.learningmosby.main.ContactsAdapter;
import tutorial.eusebiu.com.learningmosby.models.Contact;

/**
 * Created by Sebi on 4/8/2017.
 */

public class CheckContactsRequest {
    private List<Contact> contacts;

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public CheckContactsRequest(List<Contact> contacts) {
        this.contacts = contacts;
    }
}
