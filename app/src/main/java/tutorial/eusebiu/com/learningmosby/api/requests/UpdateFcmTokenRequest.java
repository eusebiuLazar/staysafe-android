package tutorial.eusebiu.com.learningmosby.api.requests;

/**
 * Created by Sebi on 5/25/2017.
 */

public class UpdateFcmTokenRequest {
    private String phoneNumber;
    private String fcmToken;

    public UpdateFcmTokenRequest(String phoneNumber, String fcmToken) {
        this.phoneNumber = phoneNumber;
        this.fcmToken = fcmToken;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
