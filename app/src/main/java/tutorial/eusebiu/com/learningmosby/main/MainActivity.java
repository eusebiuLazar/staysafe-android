package tutorial.eusebiu.com.learningmosby.main;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;

import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;


import org.greenrobot.eventbus.EventBus;
import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tutorial.eusebiu.com.learningmosby.dagger.DaggerMainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainComponent;
import tutorial.eusebiu.com.learningmosby.base.MvpBaseActivity;
import tutorial.eusebiu.com.learningmosby.dagger.MainModule;
import tutorial.eusebiu.com.learningmosby.R;
import tutorial.eusebiu.com.learningmosby.edit_profile.EditProfileActivity;
import tutorial.eusebiu.com.learningmosby.edit_profile.EditProfileView;
import tutorial.eusebiu.com.learningmosby.invite.InviteFragment;
import tutorial.eusebiu.com.learningmosby.login.LoginActivity;
import tutorial.eusebiu.com.learningmosby.models.User;
import tutorial.eusebiu.com.learningmosby.protected_contacts.ProtectedContactsFragment;
import tutorial.eusebiu.com.learningmosby.protectors.ProtectorsFragment;
import tutorial.eusebiu.com.learningmosby.splash.SplashActivity;
import tutorial.eusebiu.com.learningmosby.utils.ComplexPreferences;
import tutorial.eusebiu.com.learningmosby.utils.LoadingDialog;

public class MainActivity extends MvpBaseActivity<MainView,MainPresenter> implements MainView,LocationListener {

    MainComponent component;

    ActionBarDrawerToggle mDrawerToggle;

    @BindView(R.id.drawer)
    DrawerLayout drawer;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.fragment_container)
    FrameLayout fragmentContainer;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    @Inject
    EventBus eventBus;

    @Inject
    LoadingDialog loadingDialog;

    @Inject
    ComplexPreferences complexPreferences;

    ActionBar actionBar;

    private void initActionBar() {
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
    }

    private void setUpDrawer() {
        if (actionBar != null)
        {
            actionBar.setDisplayHomeAsUpEnabled(true);
            mDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar,R.string.anca, R.string.anca) {

                public void onDrawerClosed(View view)
                {
                    supportInvalidateOptionsMenu();
                    //drawerOpened = false;
                }

                public void onDrawerOpened(View drawerView)
                {
                    supportInvalidateOptionsMenu();
                    //drawerOpened = true;
                }
            };
            mDrawerToggle.setDrawerIndicatorEnabled(true);
            drawer.setDrawerListener(mDrawerToggle);
            mDrawerToggle.syncState();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initBaseControls();

        startLocationTracking();

        initActionBar();

        setUpFragments();

        presenter.loadContacts(getContentResolver());

        setUpDrawer();
    }

    private void startLocationTracking() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        final LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Getting the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);
        locationManager.requestLocationUpdates(provider, 500, 0, this);

        Location location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

        if (location == null)
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);   // try everything

        if (location == null)
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);   //really try everything

        if (location != null) {
            onLocationChanged(location);
        }
    }

    @OnClick (R.id.logout)
    public void clickLogout() {
        User loggedInUser = complexPreferences.getObject("user", User.class);
        loggedInUser.setPhoneNumber(null);
        complexPreferences.putObject("user", loggedInUser);

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick (R.id.edit_profile)
    public void clickEditProfile() {
        Intent intent = new Intent(this, EditProfileActivity.class);
        startActivity(intent);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @NonNull
    @Override
    public MainPresenter createPresenter() {
        return getComponent().presenter();
    }

    protected MainComponent getComponent() {
        if (component == null) {
            component = DaggerMainComponent.builder().mainModule(new MainModule(this)).build();
        }
        return component;
    }

    void initBaseControls() {
        ButterKnife.bind(this);
        getComponent().inject(this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void setUpFragments() {

        Fragment newFragment = new ProtectorsFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.fragment_container, newFragment).commit();
        actionBar.setTitle("Protectori");
        tabLayout.getTabAt(1).select();
        tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.drawable.shield));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                Fragment newFragment = new Fragment();

                switch (tab.getPosition()) {

                    case 0:
                        newFragment = new ProtectedContactsFragment();
                        actionBar.setTitle("Protejați");

                        tabLayout.getTabAt(0).setIcon(getResources().getDrawable(R.drawable.hand));
                        tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.drawable.shield_pink));
                        tabLayout.getTabAt(2).setIcon(getResources().getDrawable(R.drawable.closed_envelope_pink));

                        break;
                    case 1:
                        newFragment = new ProtectorsFragment();
                        actionBar.setTitle("Protectori");

                        tabLayout.getTabAt(0).setIcon(getResources().getDrawable(R.drawable.hand_pink));
                        tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.drawable.shield));
                        tabLayout.getTabAt(2).setIcon(getResources().getDrawable(R.drawable.closed_envelope_pink));

                        break;
                    case 2:
                        newFragment = new InviteFragment();
                        actionBar.setTitle("Invită");

                        tabLayout.getTabAt(0).setIcon(getResources().getDrawable(R.drawable.hand_pink));
                        tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.drawable.shield_pink));
                        tabLayout.getTabAt(2).setIcon(getResources().getDrawable(R.drawable.closed_envelope));

                        break;
                }

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, newFragment).commit();

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    @Override
    public void showLoading(boolean show) {
        if(show)
            loadingDialog.show();
        else
            loadingDialog.hide();
    }

    @Override
    public void onLocationChanged(Location location) {
        complexPreferences.putObject("lastKnownLocation", location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
