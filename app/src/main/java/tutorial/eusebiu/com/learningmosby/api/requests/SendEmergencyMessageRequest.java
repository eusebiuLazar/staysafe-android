package tutorial.eusebiu.com.learningmosby.api.requests;

/**
 * Created by Sebi on 5/26/2017.
 */

public class SendEmergencyMessageRequest {
    private String phoneNumber;
    private double lat;
    private double lng;
    private String address;
    private long timestamp;

    public SendEmergencyMessageRequest(String phoneNumber, double lat, double lng, String address, long timestamp) {
        this.phoneNumber = phoneNumber;
        this.lat = lat;
        this.lng = lng;
        this.address = address;
        this.timestamp = timestamp;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
