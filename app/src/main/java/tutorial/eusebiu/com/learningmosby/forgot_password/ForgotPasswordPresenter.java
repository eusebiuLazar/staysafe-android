package tutorial.eusebiu.com.learningmosby.forgot_password;

import android.widget.EditText;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import tutorial.eusebiu.com.learningmosby.api.Api;
import tutorial.eusebiu.com.learningmosby.api.requests.ForgotPasswordRequest;
import tutorial.eusebiu.com.learningmosby.api.responses.BaseResponse;

/**
 * Created by Sebi on 4/3/2017.
 */

public class ForgotPasswordPresenter extends MvpBasePresenter<ForgotPasswordView> {

    @Inject
    Api api;

    @Inject
    public ForgotPasswordPresenter() {
    }

    public void tryResetPassword(EditText phoneNumber) {

        if(validate(phoneNumber))
        {
            api.forgotPassword(new ForgotPasswordRequest(phoneNumber.getText().toString()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<BaseResponse>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            if(isViewAttached()) {
                                getView().showLoading(false);
                                getView().showToast("Conexiune nereusita!");
                            }

                        }

                        @Override
                        public void onNext(BaseResponse forgotResponse) {
                            if (isViewAttached())
                                getView().showLoading(false);

                            if(!forgotResponse.isSuccess())
                            {
                                if(isViewAttached())
                                    getView().showToast(forgotResponse.getMessage());
                            }
                            else
                            {
                                if(isViewAttached())
                                    getView().onSuccess();
                            }
                        }
                    });

        }

    }

    public boolean validate(EditText phoneNumber) {

        if(phoneNumber.getText().toString().length()!=10) {
            phoneNumber.setError("Numarul de telefon trebuie sa aiba 10 cifre!");
            return false;
        }
        return true;
    }
}
