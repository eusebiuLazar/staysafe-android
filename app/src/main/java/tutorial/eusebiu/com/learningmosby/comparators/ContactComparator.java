package tutorial.eusebiu.com.learningmosby.comparators;

import java.util.Comparator;

import tutorial.eusebiu.com.learningmosby.models.Contact;

/**
 * Created by Sebi on 5/26/2017.
 */

public class ContactComparator implements Comparator<Contact> {

    @Override
    public int compare(Contact contact, Contact t1) {

        int comparisonResult = contact.getName().compareTo(t1.getName());

        if (comparisonResult > 0)
            return 1;
        else if (comparisonResult < 0)
            return -1;
        else
            return contact.getPhoneNumber().compareTo(t1.getPhoneNumber());
    }

}
