package tutorial.eusebiu.com.learningmosby.api.requests;

import android.widget.EditText;

import tutorial.eusebiu.com.learningmosby.models.User;

/**
 * Created by Sebi on 4/23/2017.
 */

public class EditProfileRequest {
    private String phoneNumber;
    private String emergencyMessage;
    private String email;
    private String firstName;
    private String lastName;

    public EditProfileRequest(String phoneNumber, String emergencyMessage, String email, String firstName, String lastName) {
        this.phoneNumber = phoneNumber;
        this.emergencyMessage = emergencyMessage;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmergencyMessage() {
        return emergencyMessage;
    }

    public void setEmergencyMessage(String emergencyMessage) {
        this.emergencyMessage = emergencyMessage;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
