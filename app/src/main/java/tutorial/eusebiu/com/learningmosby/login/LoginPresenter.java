package tutorial.eusebiu.com.learningmosby.login;

import android.widget.EditText;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import tutorial.eusebiu.com.learningmosby.api.Api;
import tutorial.eusebiu.com.learningmosby.api.requests.LoginRequest;
import tutorial.eusebiu.com.learningmosby.api.responses.LoginResponse;
import tutorial.eusebiu.com.learningmosby.utils.ComplexPreferences;

/**
 * Created by Sebi on 3/19/2017.
 */

public class LoginPresenter extends MvpBasePresenter<LoginView> {

    @Inject
    ComplexPreferences complexPreferences;

    @Inject
    Api api;

    @Inject
    public LoginPresenter() {

    }

    public void tryLogin(EditText phoneNumber, EditText password){
        if(validateInput(phoneNumber,password)) {
            if(isViewAttached()){

                String fcmToken = complexPreferences.getObject("fcmToken", String.class);
                if (fcmToken == null)
                    fcmToken = "";

                getView().showLoading(true);
                api.login(new LoginRequest(phoneNumber.getText().toString(), password.getText().toString(), fcmToken))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<LoginResponse>() {
                            @Override
                            public void onCompleted() {
                            }

                            @Override
                            public void onError(Throwable e) {
                                if(isViewAttached()) {
                                    getView().showLoading(false);
                                    getView().showToast("Conexiune nereusita!");
                                }

                            }

                            @Override
                            public void onNext(LoginResponse loginResponse) {
                                if (isViewAttached())
                                    getView().showLoading(false);

                                if(!loginResponse.isSuccess())
                                {
                                    if(isViewAttached())
                                        getView().showToast(loginResponse.getMessage());
                                }
                                else
                                {
                                    complexPreferences.putObject("user", loginResponse.getUser());
                                    if(isViewAttached())
                                        getView().onLoginSuccess();
                                }
                            }
                        });


            }
        }


    }

    private boolean validateInput(EditText phoneNumber, EditText password){

        if(phoneNumber.getText().toString().equals("") || password.getText().equals("")){
            if(isViewAttached()){
                getView().showToast("Completati toate campurile!");

            }
            return false;

        }

        if(phoneNumber.getText().toString().length()!=10) {
            phoneNumber.setError("Numarul de telefon trebuie sa contina 10 cifre!");
            return false;
        }

        if(password.getText().toString().length()<6){
            password.setError("Parola trebuie sa aiba minim 6 caractere");
            return false;
        }

        return true;

    }
}
