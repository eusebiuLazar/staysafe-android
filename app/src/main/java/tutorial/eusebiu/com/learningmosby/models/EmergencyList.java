package tutorial.eusebiu.com.learningmosby.models;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Sebi on 4/11/2017.
 */

public class EmergencyList {
    private Set<String> phones = new HashSet<>();

    public EmergencyList(Set<String> phones) {
        this.phones = phones;
    }

    public Set<String> getPhones() {
        return phones;
    }

    public void setPhones(Set<String> phones) {
        this.phones = phones;
    }
}
