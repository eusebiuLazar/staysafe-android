package tutorial.eusebiu.com.learningmosby.map;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import tutorial.eusebiu.com.learningmosby.R;
import tutorial.eusebiu.com.learningmosby.dagger.DaggerMainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainModule;
import tutorial.eusebiu.com.learningmosby.main.MainActivity;
import tutorial.eusebiu.com.learningmosby.utils.ComplexPreferences;
import tutorial.eusebiu.com.learningmosby.utils.DateUtils;
import tutorial.eusebiu.com.learningmosby.utils.ShortMessageManager;

/**
 * Created by Sebi on 4/11/2017.
 */

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback{

//    MainComponent component;
//
//    @Inject
//    ComplexPreferences complexPreferences;
//
//    @Inject
//    ShortMessageManager shortMessageManager;
    private LatLng point;
    private String address;
    private String date;
    private String message;

    @BindView(R.id.date)
    TextView dateTv;

    @BindView(R.id.message)
    TextView messageTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_map);

        initBaseControls();

        getDataFromIntent();

        setTextViews();

        setUpMap();
    }

    private void setTextViews() {
        dateTv.setText(date);
        messageTv.setText(message);
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        point = new LatLng(bundle.getDouble("lat"), bundle.getDouble("lng"));
        address = bundle.getString("address");
        date = DateUtils.formatDate(bundle.getLong("timestamp"));
        message = bundle.getString("message");
    }

    private void setUpMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.addMarker(new MarkerOptions().position(point).title(address)).showInfoWindow();
        moveMapAtLatLng(googleMap, point);
    }

    private void moveMapAtLatLng(GoogleMap map, LatLng latLng) {
        CameraUpdate center =
                CameraUpdateFactory.newLatLngZoom(latLng, 14);

        map.moveCamera(center);
    }

//    protected MainComponent getComponent() {
//        if (component == null) {
//            component = DaggerMainComponent.builder().mainModule(new MainModule(this)).build();
//        }
//        return component;
//    }
//
    void initBaseControls() {
        ButterKnife.bind(this);
       // getComponent().inject(this);
    }
}
