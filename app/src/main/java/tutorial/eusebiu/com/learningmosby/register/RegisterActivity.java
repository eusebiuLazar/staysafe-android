package tutorial.eusebiu.com.learningmosby.register;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tutorial.eusebiu.com.learningmosby.R;
import tutorial.eusebiu.com.learningmosby.base.MvpBaseActivity;
import tutorial.eusebiu.com.learningmosby.dagger.MainModule;
import tutorial.eusebiu.com.learningmosby.dagger.DaggerMainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainComponent;
import tutorial.eusebiu.com.learningmosby.main.MainActivity;
import tutorial.eusebiu.com.learningmosby.utils.EditTextUtils;
import tutorial.eusebiu.com.learningmosby.utils.LoadingDialog;
import tutorial.eusebiu.com.learningmosby.utils.ShortMessageManager;

/**
 * Created by Sebi on 3/25/2017.
 */

public class RegisterActivity  extends MvpBaseActivity<RegisterView, RegisterPresenter> implements RegisterView {
    MainComponent component;

    @BindView(R.id.phone_number)
    EditText phoneNumber;

    @BindView(R.id.first_name)
    EditText firstName;

    @BindView(R.id.last_name)
    EditText lastName;

    @BindView(R.id.password)
    EditText password;

    @BindView(R.id.re_password)
    EditText rePassword;

    @BindView(R.id.email)
    EditText email;

    @Inject
    ShortMessageManager toastMaker;

    @Inject
    LoadingDialog loadingDialog;

    @NonNull
    @Override
    public RegisterPresenter createPresenter() {
        return getComponent().registerPresenter();
    }

    protected MainComponent getComponent() {
        if (component == null) {
            component = DaggerMainComponent.builder().mainModule(new MainModule(this)).build();
        }
        return component;
    }

    void initBaseControls() {
        ButterKnife.bind(this);
        getComponent().inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initBaseControls();

    }

    @OnClick(R.id.create_account)
    public void register() {

        trimEverything();

        presenter.tryRegister(phoneNumber, firstName, lastName, password, rePassword, email);

    }

    private void trimEverything() {
        EditTextUtils.trimEditText(phoneNumber);
        EditTextUtils.trimEditText(firstName);
        EditTextUtils.trimEditText(lastName);
        EditTextUtils.trimEditText(password);
        EditTextUtils.trimEditText(rePassword);
        EditTextUtils.trimEditText(email);
    }

    @Override
    public void showToast(String toast) {
        toastMaker.showShortMessage(toast);
    }

    @Override
    public void onRegisterSuccess() {
        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showLoading(boolean show) {
        if (show)
            loadingDialog.show();
        else
            loadingDialog.hide();
    }
}
