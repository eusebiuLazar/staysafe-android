package tutorial.eusebiu.com.learningmosby.api.responses;

import java.util.List;

import tutorial.eusebiu.com.learningmosby.models.Contact;

/**
 * Created by Sebi on 4/8/2017.
 */

public class CheckContactsResponse extends BaseResponse {
    private List<Contact> contacts;

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public CheckContactsResponse(String message, boolean success, List<Contact> contacts) {
        super(message, success);
        this.contacts = contacts;
    }
}
