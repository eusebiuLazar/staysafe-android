package tutorial.eusebiu.com.learningmosby.api.requests;

/**
 * Created by Sebi on 4/2/2017.
 */

public class LoginRequest {

    private String phoneNumber, password, fcmToken;

    public LoginRequest(String phoneNumber, String password, String fcmToken) {
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.fcmToken = fcmToken;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
