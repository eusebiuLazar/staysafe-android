package tutorial.eusebiu.com.learningmosby.edit_profile;

import android.widget.EditText;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import tutorial.eusebiu.com.learningmosby.api.Api;
import tutorial.eusebiu.com.learningmosby.api.requests.EditProfileRequest;
import tutorial.eusebiu.com.learningmosby.api.responses.LoginResponse;
import tutorial.eusebiu.com.learningmosby.models.User;
import tutorial.eusebiu.com.learningmosby.register.RegisterView;
import tutorial.eusebiu.com.learningmosby.utils.ComplexPreferences;

/**
 * Created by Sebi on 4/22/2017.
 */

public class EditProfilePresenter extends MvpBasePresenter<EditProfileView> {

    @Inject
    Api api;

    @Inject
    ComplexPreferences complexPreferences;

    @Inject
    public EditProfilePresenter() {
    }

    public void tryEditProfile(EditText phoneNumber, EditText emergencyMessage, EditText email, EditText firstName, EditText lastName) {
        if (validateData(phoneNumber, email, firstName, lastName)) {
            EditProfileRequest editProfileRequest = new EditProfileRequest(phoneNumber.getText().toString(), emergencyMessage.getText().toString(), email.getText().toString(), firstName.getText().toString(), lastName.getText().toString());

            if (isViewAttached())
                getView().showLoading(true);

            api.editProfile(editProfileRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<LoginResponse>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            if(isViewAttached()) {
                                getView().showLoading(false);
                                getView().showToast("Conexiune nereusita!");
                            }
                        }

                        @Override
                        public void onNext(LoginResponse loginResponse) {
                            if (isViewAttached())
                                getView().showLoading(false);

                            if(!loginResponse.isSuccess())
                            {
                                if(isViewAttached())
                                    getView().showToast(loginResponse.getMessage());
                            }
                            else
                            {
                                complexPreferences.putObject("user", loginResponse.getUser());
                                if(isViewAttached()) {
                                    getView().showToast(loginResponse.getMessage());
                                    getView().setData();
                                }
                            }
                        }
                    });
        }
    }

    private boolean validateData(EditText phoneNumber, EditText email, EditText firstName, EditText lastName) {
        if(phoneNumber.getText().toString().equals("") || firstName.getText().toString().equals("") || lastName.getText().toString().equals("") || email.getText().toString().equals(""))
        {
            if(isViewAttached())
                getView().showToast("Completati toate campurile!");
            return false;
        }

        if(phoneNumber.getText().toString().length()!=10) {
            phoneNumber.setError("Numarul de telefon trebuie sa contina 10 cifre!");
            return false;
        }

        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            email.setError("Introduceti o adresa de email valida!");
            return false;
        }

        return true;
    }
}
