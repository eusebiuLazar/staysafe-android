package tutorial.eusebiu.com.learningmosby.api.requests;

import java.util.List;

import tutorial.eusebiu.com.learningmosby.models.Contact;

/**
 * Created by Sebi on 4/10/2017.
 */

public class SendRequestToProtectorsRequest {
    private String phoneNumber;
    List<Contact> protectors;

    public SendRequestToProtectorsRequest(String phoneNumber, List<Contact> protectors) {
        this.phoneNumber = phoneNumber;
        this.protectors = protectors;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<Contact> getProtectors() {
        return protectors;
    }

    public void setProtectors(List<Contact> protectors) {
        this.protectors = protectors;
    }
}
