package tutorial.eusebiu.com.learningmosby.invite;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby.mvp.MvpFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import tutorial.eusebiu.com.learningmosby.R;
import tutorial.eusebiu.com.learningmosby.dagger.DaggerMainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainModule;
import tutorial.eusebiu.com.learningmosby.main.ContactsAdapter;
import tutorial.eusebiu.com.learningmosby.models.Contact;
import tutorial.eusebiu.com.learningmosby.navigation.AcceptProtectedEvent;
import tutorial.eusebiu.com.learningmosby.navigation.ContactsInsertedEvent;
import tutorial.eusebiu.com.learningmosby.protectors.ProtectorsPresenter;
import tutorial.eusebiu.com.learningmosby.protectors.ProtectorsView;
import tutorial.eusebiu.com.learningmosby.utils.LoadingDialog;

/**
 * Created by Sebi on 3/27/2017.
 */

public class InviteFragment extends MvpFragment<InviteView, InvitePresenter> implements InviteView {

    MainComponent component;
    View view;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    ContactsAdapter adapter;

    @Inject
    LoadingDialog loadingDialog;

    @Inject
    EventBus eventBus;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_invite, null);

        return view;


    }

    @Subscribe
    public void onEvent(ContactsInsertedEvent event) {
        presenter.getContactsFromLocalDB();

    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initBaseControls();
        if(!eventBus.isRegistered(this))
            eventBus.register(this);
        setFields();
        presenter.getContactsFromLocalDB();

    }

    protected MainComponent getComponent() {
        if (component == null) {
            component = DaggerMainComponent.builder().mainModule(new MainModule(this.getContext())).build();
        }
        return component;
    }

    void initBaseControls() {
        ButterKnife.bind(this, view);
        getComponent().inject(this);
    }

    private void setFields() {
        adapter = getComponent().contactsAdapter();
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public InvitePresenter createPresenter() {
        return getComponent().invitePresenter();
    }

    @Override
    public void showLoading(boolean show) {

        if(show == true)
            loadingDialog.show();
        else
            loadingDialog.hide();
    }

    @Override
    public void setAdapterData(List<Contact> list) {
        adapter.setmDataset(list);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(eventBus.isRegistered(this))
            eventBus.unregister(this);
    }
}

