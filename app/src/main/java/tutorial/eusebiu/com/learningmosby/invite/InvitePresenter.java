package tutorial.eusebiu.com.learningmosby.invite;

import android.content.ContentResolver;
import android.os.AsyncTask;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import tutorial.eusebiu.com.learningmosby.api.Api;
import tutorial.eusebiu.com.learningmosby.api.requests.CheckContactsRequest;
import tutorial.eusebiu.com.learningmosby.api.responses.CheckContactsResponse;
import tutorial.eusebiu.com.learningmosby.models.Contact;
import tutorial.eusebiu.com.learningmosby.navigation.ContactsInsertedEvent;
import tutorial.eusebiu.com.learningmosby.protectors.ProtectorsView;
import tutorial.eusebiu.com.learningmosby.utils.PhoneUtils;

/**
 * Created by Sebi on 3/27/2017.
 */

public class InvitePresenter extends MvpBasePresenter<InviteView> {

    @Inject
    Api api;

    @Inject
    public InvitePresenter() {
    }

    public void getContactsFromLocalDB() {

        new AsyncTask<Void, Void, List<Contact>>() {

            @Override
            protected List<Contact> doInBackground(Void... voids) {
                return Contact.findWithQuery(Contact.class, "SELECT * FROM Contact");
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if(isViewAttached())
                    getView().showLoading(true);

            }

            @Override
            protected void onPostExecute(List<Contact> contacts) {
                super.onPostExecute(contacts);

                checkContactsWithServer(contacts);

                if(isViewAttached()) {
                    getView().setAdapterData(contacts);
                    getView().showLoading(false);
                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


    }

    public void checkContactsWithServer(List<Contact> contacts) {
        api.checkContacts(new CheckContactsRequest(contacts))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CheckContactsResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(CheckContactsResponse checkContactsResponse) {
                        if (checkContactsResponse.isSuccess()) {
                            updateContacts(checkContactsResponse.getContacts());
                        }
                    }
                });
    }

    private void updateContacts(final List<Contact> contacts) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                for (Contact contact:contacts) {
                    contact.save();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void v) {
                super.onPostExecute(v);
                if (isViewAttached())
                    getView().setAdapterData(contacts);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }



}
