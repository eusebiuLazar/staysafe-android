package tutorial.eusebiu.com.learningmosby.utils;

import android.content.Context;
import android.widget.Toast;

public class ShortMessageManager {

    Context context;

    public ShortMessageManager(Context context)
    {
        this.context = context;
    }

    public void showShortMessage(String message)
    {
        if (message == null)
            return;

        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public void showShortMessage(int stringId)
    {
        Toast.makeText(context, context.getResources().getString(stringId), Toast.LENGTH_LONG).show();
    }
}
