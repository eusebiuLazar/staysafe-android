package tutorial.eusebiu.com.learningmosby.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Sebi on 5/27/2017.
 */

public class DateUtils {
    public static String formatDate(long timestamp) {
        Date date = new Date(timestamp);
        DateFormat f = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.getDefault());
        return f.format(date);
    }
}
