package tutorial.eusebiu.com.learningmosby.request_permissions;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import tutorial.eusebiu.com.learningmosby.R;
import tutorial.eusebiu.com.learningmosby.login.LoginActivity;

/**
 * Created by Sebi on 3/25/2017.
 */

public class RequestPermissionsActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 21;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_permissions);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.permissions_button)
    public void requestPermissions() {
        ActivityCompat.requestPermissions(RequestPermissionsActivity.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_CONTACTS, Manifest.permission.SEND_SMS},MY_PERMISSIONS_REQUEST_READ_CONTACTS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {

                boolean arePermissionsGranted = true;
                for(int i = 0; i < grantResults.length; i++) {
                    if(grantResults[i] == PackageManager.PERMISSION_DENIED)
                        arePermissionsGranted = false;
                }

                if(arePermissionsGranted){
                    Intent intent = new Intent(RequestPermissionsActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }



                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


}
