package tutorial.eusebiu.com.learningmosby.api.requests;

/**
 * Created by Sebi on 4/9/2017.
 */

public class RelationshipRequest {

    private String userPhoneNumber, contactPhoneNumber;

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }

    public String getContactPhoneNumber() {
        return contactPhoneNumber;
    }

    public void setContactPhoneNumber(String contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
    }

    public RelationshipRequest(String userPhoneNumber, String contactPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
        this.contactPhoneNumber = contactPhoneNumber;
    }
}
