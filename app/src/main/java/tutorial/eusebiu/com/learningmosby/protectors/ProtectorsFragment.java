package tutorial.eusebiu.com.learningmosby.protectors;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.hannesdorfmann.mosby.mvp.MvpFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tutorial.eusebiu.com.learningmosby.R;
import tutorial.eusebiu.com.learningmosby.dagger.DaggerMainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainModule;
import tutorial.eusebiu.com.learningmosby.main.ContactsAdapter;
import tutorial.eusebiu.com.learningmosby.models.Contact;
import tutorial.eusebiu.com.learningmosby.models.User;
import tutorial.eusebiu.com.learningmosby.navigation.DeleteProtectedEvent;
import tutorial.eusebiu.com.learningmosby.utils.ComplexPreferences;
import tutorial.eusebiu.com.learningmosby.utils.LoadingDialog;
import tutorial.eusebiu.com.learningmosby.utils.ShortMessageManager;

/**
 * Created by Sebi on 3/27/2017.
 */

public class ProtectorsFragment extends MvpFragment<ProtectorsView, ProtectorsPresenter> implements ProtectorsView {

    @Inject
    ComplexPreferences complexPreferences;

    @Inject
    EventBus eventBus;

    @Inject
    ShortMessageManager toastMaker;

    @Inject
    LoadingDialog loadingDialog;

    @Inject
    Context context;

    MainComponent component;
    View view;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    ContactsAdapter adapter;

    AddProtectorsAdapter addProtectorsAdapter;

    RecyclerView recyclerViewDialog;

    User user;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_protectors, null);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initBaseControls();
        if(!eventBus.isRegistered(this))
            eventBus.register(this);
        setFields();
        user = complexPreferences.getObject("user", User.class);
        presenter.tryGetProtectors(user.getPhoneNumber());
    }

    @OnClick (R.id.fab)
    public void clickAddProtector() {

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        View dialogProtectors = getActivity().getLayoutInflater().inflate(R.layout.add_protectors_dialog, null);
        recyclerViewDialog = (RecyclerView) dialogProtectors.findViewById(R.id.recycler_view_dialog);
        EditText searchString = (EditText) dialogProtectors.findViewById(R.id.searchTextBox);
        LinearLayoutManager llm = new LinearLayoutManager(context);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewDialog.setLayoutManager(llm);
        recyclerViewDialog.setAdapter(addProtectorsAdapter);

        searchString.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                presenter.getContactsFromLocalDB(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        builderSingle.setView(dialogProtectors);

        builderSingle.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                presenter.trySendRequestToProtectors(user.getPhoneNumber(), addProtectorsAdapter.getSelectedContacts());
            }
        });

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.show();

        presenter.getContactsFromLocalDB("");

    }

    @Subscribe
    public void onEvent(DeleteProtectedEvent event) {
        presenter.tryDeleteProtector(complexPreferences.getObject("user", User.class).getPhoneNumber(), event.getContactPhoneNumber());

    }

    private void setFields() {
        adapter = getComponent().contactsAdapter();
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);

        addProtectorsAdapter = getComponent().addProtectorsAdapter();
    }

    protected MainComponent getComponent() {
        if (component == null) {
            component = DaggerMainComponent.builder().mainModule(new MainModule(getActivity())).build();
        }
        return component;
    }

    void initBaseControls() {
        ButterKnife.bind(this,view);
        getComponent().inject(this);
    }

    @Override
    public ProtectorsPresenter createPresenter() {
        return getComponent().protectorsPresenter();
    }

    @Override
    public void showLoading(boolean show) {

        if(show)
            loadingDialog.show();
        else
            loadingDialog.hide();
    }

    @Override
    public void showToast(String toast) {
        toastMaker.showShortMessage(toast);
    }

    @Override
    public void setDialogAdapterData(List<Contact> list) {
        addProtectorsAdapter.setDataset(list);
    }

    @Override
    public void setAdapterData(List<Contact> list) {
        adapter.setmDataset(list);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(eventBus.isRegistered(this))
            eventBus.unregister(this);
    }
}
