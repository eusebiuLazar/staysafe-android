package tutorial.eusebiu.com.learningmosby.login;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tutorial.eusebiu.com.learningmosby.R;
import tutorial.eusebiu.com.learningmosby.base.MvpBaseActivity;
import tutorial.eusebiu.com.learningmosby.dagger.MainModule;
import tutorial.eusebiu.com.learningmosby.dagger.DaggerMainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainComponent;
import tutorial.eusebiu.com.learningmosby.forgot_password.ForgotPasswordActivity;
import tutorial.eusebiu.com.learningmosby.main.MainActivity;
import tutorial.eusebiu.com.learningmosby.receivers.LockReceiver;
import tutorial.eusebiu.com.learningmosby.register.RegisterActivity;
import tutorial.eusebiu.com.learningmosby.utils.EditTextUtils;
import tutorial.eusebiu.com.learningmosby.utils.LoadingDialog;
import tutorial.eusebiu.com.learningmosby.utils.ShortMessageManager;

/**
 * Created by Sebi on 3/19/2017.
 */

public class LoginActivity extends MvpBaseActivity<LoginView, LoginPresenter> implements LoginView {

    @Inject
    ShortMessageManager toastMaker;

    MainComponent component;

    @Inject
    LoadingDialog loadingDialog;

    @BindView(R.id.phone_number)
    EditText phoneNumber;

    @BindView(R.id.password)
    EditText password;

    @OnClick(R.id.login_button)
    public void tryLogin() {

        trimEverything();

        presenter.tryLogin(phoneNumber,password);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initBaseControls();

    }

    private void trimEverything() {
        EditTextUtils.trimEditText(phoneNumber);
        EditTextUtils.trimEditText(password);
    }

    @OnClick(R.id.forgot_password_label)
    public void goToForgotPassword() {
        Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    @NonNull
    @Override
    public LoginPresenter createPresenter() {
        return getComponent().loginPresenter();
    }

    protected MainComponent getComponent() {
        if (component == null) {
            component = DaggerMainComponent.builder().mainModule(new MainModule(this)).build();
        }
        return component;
    }

    void initBaseControls() {
        ButterKnife.bind(this);
        getComponent().inject(this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onLoginSuccess() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void OnError() {

    }

    @Override
    public void showLoading(boolean show) {
        if(show){
            loadingDialog.show();
        }
        else
            loadingDialog.hide();
    }

    @Override
    public void showToast(String toast) {
        toastMaker.showShortMessage(toast);
    }

    @OnClick(R.id.signup_label)
    public void goToRegister(){
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }
}

