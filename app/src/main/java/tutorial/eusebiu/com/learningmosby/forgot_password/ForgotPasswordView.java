package tutorial.eusebiu.com.learningmosby.forgot_password;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Created by Sebi on 4/3/2017.
 */

public interface ForgotPasswordView extends MvpView {

    void showToast(String toast);

    void showLoading(boolean show);

    void onSuccess();
}
