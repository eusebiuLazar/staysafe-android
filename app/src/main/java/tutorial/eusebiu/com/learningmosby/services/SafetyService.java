package tutorial.eusebiu.com.learningmosby.services;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;

import tutorial.eusebiu.com.learningmosby.receivers.LockReceiver;

/**
 * Created by Sebi on 4/17/2017.
 */

public class SafetyService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
        // login receiver that handles screen on and screen off logic
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        LockReceiver mReceiver = new LockReceiver(getApplicationContext());
        registerReceiver(mReceiver, filter);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}