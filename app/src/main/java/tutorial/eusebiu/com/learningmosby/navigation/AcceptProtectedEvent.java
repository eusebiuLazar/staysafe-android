package tutorial.eusebiu.com.learningmosby.navigation;

/**
 * Created by Sebi on 4/9/2017.
 */

public class AcceptProtectedEvent {

    String contactPhoneNumber;

    public AcceptProtectedEvent(String contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
    }

    public String getContactPhoneNumber() {
        return contactPhoneNumber;
    }

    public void setContactPhoneNumber(String contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
    }
}
