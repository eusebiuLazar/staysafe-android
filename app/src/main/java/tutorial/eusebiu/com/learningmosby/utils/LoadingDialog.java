package tutorial.eusebiu.com.learningmosby.utils;

import android.app.ProgressDialog;
import android.content.Context;

public class LoadingDialog {

    private ProgressDialog mLoadingDialog;
    private Context context;


    public LoadingDialog(Context context) {
        this.context = context;
    }

    public void show()
    {
        try
        {
            if (mLoadingDialog != null)
            {
                mLoadingDialog.dismiss();
                mLoadingDialog = null;
            }
            mLoadingDialog = createAndShow();
        }
        catch (Exception e)
        {

        }
    }

    public void hide()
    {
        try {
            mLoadingDialog.dismiss();
        } catch (Exception e) {

        }
    }

    private ProgressDialog createAndShow()
    {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setCancelable(false);
        dialog.show();
        return dialog;
    }
}
