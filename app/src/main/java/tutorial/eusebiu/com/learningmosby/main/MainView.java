package tutorial.eusebiu.com.learningmosby.main;

import com.hannesdorfmann.mosby.mvp.MvpView;

import java.util.List;

import tutorial.eusebiu.com.learningmosby.models.Contact;

/**
 * Created by mihai on 05/12/16.
 */

public interface MainView extends MvpView {

    void showLoading(boolean show);


}
