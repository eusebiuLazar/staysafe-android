package tutorial.eusebiu.com.learningmosby.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Country implements Serializable {
    @SerializedName("code")
    public String code;

    @SerializedName("name")
    public String name;
}
