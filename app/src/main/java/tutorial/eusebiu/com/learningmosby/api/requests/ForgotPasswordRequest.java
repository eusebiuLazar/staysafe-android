package tutorial.eusebiu.com.learningmosby.api.requests;

/**
 * Created by Sebi on 4/3/2017.
 */

public class ForgotPasswordRequest {

    private String phoneNumber;

    public ForgotPasswordRequest(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
