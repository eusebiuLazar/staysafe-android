package tutorial.eusebiu.com.learningmosby.splash;

import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;
import tutorial.eusebiu.com.learningmosby.R;
import tutorial.eusebiu.com.learningmosby.dagger.DaggerMainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainModule;
import tutorial.eusebiu.com.learningmosby.login.LoginActivity;
import tutorial.eusebiu.com.learningmosby.main.MainActivity;
import tutorial.eusebiu.com.learningmosby.models.User;
import tutorial.eusebiu.com.learningmosby.receivers.LockReceiver;
import tutorial.eusebiu.com.learningmosby.request_permissions.RequestPermissionsActivity;
import tutorial.eusebiu.com.learningmosby.services.SafetyService;
import tutorial.eusebiu.com.learningmosby.utils.ComplexPreferences;

/**
 * Created by Sebi on 3/25/2017.
 */

public class SplashActivity extends AppCompatActivity {

    MainComponent component;

    @Inject
    ComplexPreferences complexPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initBaseControls();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if(!checkLocationPermission()){
                    Intent intent = new Intent(SplashActivity.this, RequestPermissionsActivity.class);
                    startActivity(intent);
                    finish();
                }
                else {

                    if (!isUserLoggedIn()) {
                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }

                }

            }
        }, 1500);

        Intent intent = new Intent(this, SafetyService.class);
        startService(intent);
    }

    public boolean isUserLoggedIn() {
        User loggedInUser = complexPreferences.getObject("user", User.class);

        if (loggedInUser == null)
            return false;

        if (loggedInUser.getPhoneNumber() == null)
            return false;

        return true;
    }

    void initBaseControls() {
        ButterKnife.bind(this);
        getComponent().inject(this);
    }

    protected MainComponent getComponent() {
        if (component == null) {
            component = DaggerMainComponent.builder().mainModule(new MainModule(this)).build();
        }
        return component;
    }


    public boolean checkLocationPermission(){

        int permissionCheck = ContextCompat.checkSelfPermission(SplashActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        int permissionCheck2 = ContextCompat.checkSelfPermission(SplashActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionCheck3 = ContextCompat.checkSelfPermission(SplashActivity.this,
                Manifest.permission.READ_CONTACTS);
        int permissionCheck4 = ContextCompat.checkSelfPermission(SplashActivity.this,
                Manifest.permission.SEND_SMS);


        if(permissionCheck != PackageManager.PERMISSION_GRANTED || permissionCheck2 != PackageManager.PERMISSION_GRANTED || permissionCheck3 != PackageManager.PERMISSION_GRANTED || permissionCheck4 != PackageManager.PERMISSION_GRANTED)
            return false;

        return true;


    }


}
