package tutorial.eusebiu.com.learningmosby.register;

import android.widget.EditText;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import tutorial.eusebiu.com.learningmosby.api.Api;
import tutorial.eusebiu.com.learningmosby.api.requests.RegisterRequest;
import tutorial.eusebiu.com.learningmosby.api.responses.LoginResponse;
import tutorial.eusebiu.com.learningmosby.utils.ComplexPreferences;

/**
 * Created by Sebi on 3/25/2017.
 */

public class RegisterPresenter  extends MvpBasePresenter<RegisterView> {

    @Inject
    ComplexPreferences complexPreferences;

    @Inject
    Api api;

    @Inject
    public RegisterPresenter() {
    }

    public void tryRegister(EditText phoneNumber, EditText firstName, EditText lastName, EditText password, EditText rePassword, EditText email){


        if(validateRegister(phoneNumber, firstName, lastName, password, rePassword, email)){

            String fcmToken = complexPreferences.getObject("fcmToken", String.class);
            if (fcmToken == null)
                fcmToken = "";

            if (isViewAttached())
                getView().showLoading(true);
            api.register(new RegisterRequest(phoneNumber.getText().toString(), firstName.getText().toString(), lastName.getText().toString(), password.getText().toString(), email.getText().toString(), fcmToken))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<LoginResponse>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            if(isViewAttached()) {
                                getView().showLoading(false);
                                getView().showToast("Conexiune nereusita!");
                            }

                        }

                        @Override
                        public void onNext(LoginResponse loginResponse) {
                            if (isViewAttached())
                                getView().showLoading(false);

                            if(!loginResponse.isSuccess())
                            {
                                if(isViewAttached())
                                    getView().showToast(loginResponse.getMessage());
                            }
                            else
                            {
                                complexPreferences.putObject("user", loginResponse.getUser());
                                if(isViewAttached())
                                    getView().onRegisterSuccess();
                            }
                        }
                    });
        }

    }

    private boolean validateRegister(EditText phoneNumber, EditText firstName, EditText lastName, EditText password, EditText rePassword, EditText email) {
        if(phoneNumber.getText().toString().equals("") || firstName.getText().toString().equals("") || lastName.getText().toString().equals("") || password.getText().toString().equals("") || rePassword.getText().toString().equals("") || email.getText().toString().equals(""))
        {
            if(isViewAttached())
                getView().showToast("Completati toate campurile!");
            return false;
        }

        if(phoneNumber.getText().toString().length()!=10) {
            phoneNumber.setError("Numarul de telefon trebuie sa contina 10 cifre!");
            return false;
        }

        if(password.getText().toString().length()<6){
            password.setError("Parola trebuie sa aiba minim 6 caractere");
            return false;
        }

        if(!password.getText().toString().equals(rePassword.getText().toString())){
            rePassword.setError("Parolele nu corespund!");
            return false;
        }

        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            email.setError("Introduceti o adresa de email valida!");
            return false;
        }

        return true;
    }
}
