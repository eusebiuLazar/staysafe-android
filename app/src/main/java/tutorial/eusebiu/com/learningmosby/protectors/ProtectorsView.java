package tutorial.eusebiu.com.learningmosby.protectors;

import com.hannesdorfmann.mosby.mvp.MvpView;

import java.util.List;

import tutorial.eusebiu.com.learningmosby.models.Contact;

/**
 * Created by Sebi on 3/27/2017.
 */

public interface ProtectorsView extends MvpView {

    void showLoading(boolean show);

    void showToast(String toast);

    void setDialogAdapterData(List<Contact> list);

    void setAdapterData(List<Contact> list);




}
