package tutorial.eusebiu.com.learningmosby.forgot_password;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tutorial.eusebiu.com.learningmosby.R;
import tutorial.eusebiu.com.learningmosby.base.MvpBaseActivity;
import tutorial.eusebiu.com.learningmosby.dagger.DaggerMainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainModule;
import tutorial.eusebiu.com.learningmosby.login.LoginPresenter;
import tutorial.eusebiu.com.learningmosby.login.LoginView;
import tutorial.eusebiu.com.learningmosby.utils.LoadingDialog;

/**
 * Created by Sebi on 4/3/2017.
 */

public class ForgotPasswordActivity extends MvpBaseActivity<ForgotPasswordView, ForgotPasswordPresenter>implements ForgotPasswordView{


    @Inject
    LoadingDialog loadingDialog;

    MainComponent component;

    @BindView(R.id.phone_number)
    EditText phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        initBaseControls();

    }

    @OnClick(R.id.reset_button)
    public void resetPassword() {

        presenter.tryResetPassword(phoneNumber);
    }

    @NonNull
    @Override
    public ForgotPasswordPresenter createPresenter() {
        return getComponent().forgotPasswordPresenter();
    }

    protected MainComponent getComponent() {
        if (component == null) {
            component = DaggerMainComponent.builder().mainModule(new MainModule(this)).build();
        }
        return component;
    }

    void initBaseControls() {
        ButterKnife.bind(this);
        getComponent().inject(this);
    }

    @Override
    public void showToast(String toast) {
        Toast.makeText(this, toast, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(boolean show) {

        if(show == true)
            loadingDialog.show();
        else
            loadingDialog.hide();

    }

    @Override
    public void onSuccess() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setMessage("O noua parola va fi generata si trimisa pe adresa dumneavoastra de email!")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ForgotPasswordActivity.this.finish();
                    }
                })
                .create();
        dialog.show();
    }
}
