package tutorial.eusebiu.com.learningmosby.main;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import tutorial.eusebiu.com.learningmosby.R;
import tutorial.eusebiu.com.learningmosby.comparators.ContactComparator;
import tutorial.eusebiu.com.learningmosby.models.Contact;
import tutorial.eusebiu.com.learningmosby.navigation.AcceptProtectedEvent;
import tutorial.eusebiu.com.learningmosby.navigation.DeleteProtectedEvent;
import tutorial.eusebiu.com.learningmosby.navigation.SendRequestToProtectorEvent;

/**
 * Created by Sebi on 4/8/2017.
 */

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ContactsViewHolder>{

    @Inject
    EventBus eventBus;

    @Inject
    public ContactsAdapter() {

    }

    @Inject
    Context context;

    public void setmDataset(List<Contact> mDataset) {
        Collections.sort(mDataset, new ContactComparator());
        this.mDataset = mDataset;
        notifyDataSetChanged();
    }

    private List<Contact> mDataset = new ArrayList<>();



    // Create new views (invoked by the layout manager)
    @Override
    public ContactsViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_cell, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ContactsViewHolder vh = new ContactsViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ContactsViewHolder holder, int position) {

        final Contact contact = mDataset.get(position);
        holder.phoneNumber.setText(contact.getPhoneNumber());
        holder.name.setText(contact.getName());

        switch (contact.getStatus()) {
            case Contact.STATUS_ANONYMOUS:
                holder.layout.setVisibility(View.GONE);
                holder.button.setVisibility(View.VISIBLE);
                holder.button.setImageResource(R.drawable.ic_email_black_24dp);
                holder.button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( "sms:" + contact.getPhoneNumber()));
                        intent.putExtra( "sms_body", "Instalează-ți aplicația StaySafe!" );
                        context.startActivity(intent);
                    }
                });
                break;
            case Contact.STATUS_USER:
                holder.layout.setVisibility(View.GONE);
                holder.button.setVisibility(View.VISIBLE);
                holder.button.setImageResource(R.drawable.ic_check_circle_black_24dp);
                break;
            case Contact.STATUS_PENDING:
                holder.layout.setVisibility(View.GONE);
                holder.button.setVisibility(View.VISIBLE);
                holder.button.setImageResource(R.drawable.ic_watch_later_black_24dp);
                break;
            case Contact.STATUS_ACCEPTED:
                holder.layout.setVisibility(View.GONE);
                holder.button.setVisibility(View.VISIBLE);
                holder.button.setImageResource(R.drawable.ic_remove_circle_black_24dp);
                holder.button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        eventBus.post(new DeleteProtectedEvent(contact.getPhoneNumber()));
                    }
                });
                break;
            case Contact.STATUS_REQUEST:
                holder.button.setVisibility(View.GONE);
                holder.layout.setVisibility(View.VISIBLE);
                holder.confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        eventBus.post(new AcceptProtectedEvent(contact.getPhoneNumber()));

                    }
                });

                holder.delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        eventBus.post(new DeleteProtectedEvent(contact.getPhoneNumber()));
                    }
                });
                break;
            default:
                break;
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    static class ContactsViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView name, phoneNumber;
        ImageView button;
        LinearLayout layout;
        Button confirm;
        Button delete;

        public ContactsViewHolder(View view) {

            super(view);
            imageView =(ImageView) view.findViewById(R.id.avatar);
            name = (TextView) view.findViewById(R.id.name);
            phoneNumber = (TextView) view.findViewById(R.id.phone_number);
            button = (ImageView) view.findViewById(R.id.button);
            layout = (LinearLayout) view.findViewById(R.id.layout);
            confirm = (Button) view.findViewById(R.id.confirm);
            delete = (Button) view.findViewById(R.id.delete);
        }



    }

}
