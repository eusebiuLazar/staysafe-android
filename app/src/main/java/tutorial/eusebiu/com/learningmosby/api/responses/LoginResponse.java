package tutorial.eusebiu.com.learningmosby.api.responses;

import tutorial.eusebiu.com.learningmosby.models.User;

/**
 * Created by Sebi on 4/2/2017.
 */

public class LoginResponse extends BaseResponse {

    private User user;
    public LoginResponse(String message, boolean success, User user) {
        super(message, success);
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
