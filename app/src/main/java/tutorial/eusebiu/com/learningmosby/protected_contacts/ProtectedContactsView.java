package tutorial.eusebiu.com.learningmosby.protected_contacts;

import com.hannesdorfmann.mosby.mvp.MvpView;

import java.util.List;

import tutorial.eusebiu.com.learningmosby.models.Contact;

/**
 * Created by Sebi on 3/27/2017.
 */

public interface ProtectedContactsView extends MvpView {

    void showLoading(boolean show);

    void setAdapterData(List<Contact> list);

    void showToast(String toast);




}
