package tutorial.eusebiu.com.learningmosby.api;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;
import tutorial.eusebiu.com.learningmosby.api.requests.CheckContactsRequest;
import tutorial.eusebiu.com.learningmosby.api.requests.EditProfileRequest;
import tutorial.eusebiu.com.learningmosby.api.requests.ForgotPasswordRequest;
import tutorial.eusebiu.com.learningmosby.api.requests.GetProtectorsRequest;
import tutorial.eusebiu.com.learningmosby.api.requests.LoginRequest;
import tutorial.eusebiu.com.learningmosby.api.requests.RegisterRequest;
import tutorial.eusebiu.com.learningmosby.api.requests.RelationshipRequest;
import tutorial.eusebiu.com.learningmosby.api.requests.SendEmergencyMessageRequest;
import tutorial.eusebiu.com.learningmosby.api.requests.SendRequestToProtectorsRequest;
import tutorial.eusebiu.com.learningmosby.api.requests.UpdateFcmTokenRequest;
import tutorial.eusebiu.com.learningmosby.api.responses.BaseResponse;
import tutorial.eusebiu.com.learningmosby.api.responses.CheckContactsResponse;
import tutorial.eusebiu.com.learningmosby.api.responses.GetProtectorsResponse;
import tutorial.eusebiu.com.learningmosby.api.responses.LoginResponse;

/**
 * Created by mihai on 05/12/16.
 */


public interface Api {
    @POST("users/register")
    Observable<LoginResponse> register(@Body RegisterRequest registerRequest);

    @POST("users/login")
    Observable<LoginResponse> login(@Body LoginRequest loginRequest);

    @POST("users/forgot_password")
    Observable<BaseResponse> forgotPassword(@Body ForgotPasswordRequest forgotPasswordRequest);

    @POST("contacts/check_contacts")
    Observable<CheckContactsResponse> checkContacts(@Body CheckContactsRequest checkContactsRequest);

    @POST("contacts/get_protectors")
    Observable<GetProtectorsResponse> getProtectors(@Body GetProtectorsRequest getProtectorsRequest);

    @POST("contacts/get_protected")
    Observable<GetProtectorsResponse> getProtected(@Body GetProtectorsRequest getProtectingRequest);

    @POST("contacts/send_request_to_protectors")
    Observable<BaseResponse> sendRequestToProtectors(@Body SendRequestToProtectorsRequest sendRequestToProtectorsRequest);

    @POST("contacts/delete_protector")
    Observable<BaseResponse> deleteProtector(@Body RelationshipRequest relationshipRequest);

    @POST("contacts/delete_protected")
    Observable<BaseResponse> deleteProtected(@Body RelationshipRequest relationshipRequest);

    @POST("contacts/confirm_protected")
    Observable<BaseResponse> confirmProtected(@Body RelationshipRequest relationshipRequest);

    @POST("users/edit_profile")
    Observable<LoginResponse> editProfile(@Body EditProfileRequest loginRequest);

    @POST("users/update_fcm_token")
    Observable<BaseResponse> updateFcmToken(@Body UpdateFcmTokenRequest updateFcmTokenRequest);

    @POST("emergency_messages/send_emergency_message")
    Observable<BaseResponse> sendEmergencyMessage(@Body SendEmergencyMessageRequest emergencyMessageRequest);
}
