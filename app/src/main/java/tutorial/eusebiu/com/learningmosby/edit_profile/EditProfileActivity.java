package tutorial.eusebiu.com.learningmosby.edit_profile;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tutorial.eusebiu.com.learningmosby.R;
import tutorial.eusebiu.com.learningmosby.base.MvpBaseActivity;
import tutorial.eusebiu.com.learningmosby.dagger.DaggerMainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainModule;
import tutorial.eusebiu.com.learningmosby.models.User;
import tutorial.eusebiu.com.learningmosby.utils.ComplexPreferences;
import tutorial.eusebiu.com.learningmosby.utils.EditTextUtils;
import tutorial.eusebiu.com.learningmosby.utils.LoadingDialog;
import tutorial.eusebiu.com.learningmosby.utils.ShortMessageManager;


public class EditProfileActivity  extends MvpBaseActivity<EditProfileView, EditProfilePresenter> implements EditProfileView {

    private static final int maxLength = 150;

    MainComponent component;

    @BindView(R.id.phone_number)
    EditText phoneNumber;

    @BindView(R.id.first_name)
    EditText firstName;

    @BindView(R.id.last_name)
    EditText lastName;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.emergency)
    EditText emergencyMessage;

    @BindView(R.id.chars_remaining)
    TextView charsRemaining;

    @Inject
    ShortMessageManager toastMaker;

    @Inject
    LoadingDialog loadingDialog;

    @Inject
    ComplexPreferences complexPreferences;

    @NonNull
    @Override
    public EditProfilePresenter createPresenter() {
        return getComponent().editProfilePresenter();
    }

    protected MainComponent getComponent() {
        if (component == null) {
            component = DaggerMainComponent.builder().mainModule(new MainModule(this)).build();
        }
        return component;
    }

    void initBaseControls() {
        ButterKnife.bind(this);
        getComponent().inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        initBaseControls();

        setUpViews();
        setData();

    }

    private void setUpViews() {

        charsRemaining.setText(String.valueOf(maxLength));

        emergencyMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int remaining = maxLength - charSequence.length();
                charsRemaining.setText(String.valueOf(remaining));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public void showToast(String toast) {
        toastMaker.showShortMessage(toast);
    }

    @Override
    public void showLoading(boolean show) {
        if (show)
            loadingDialog.show();
        else
            loadingDialog.hide();
    }

    public void setData() {
        User user = complexPreferences.getObject("user", User.class);
        phoneNumber.setText(user.getPhoneNumber());
        firstName.setText(user.getFirstName());
        lastName.setText(user.getLastName());
        email.setText(user.getEmail());
        emergencyMessage.setText(user.getEmergencyMessage());
    }

    @OnClick (R.id.save)
    public void editProfile() {

        trimEverything();

        presenter.tryEditProfile(phoneNumber, emergencyMessage, email, firstName, lastName);
    }

    private void trimEverything() {
        EditTextUtils.trimEditText(phoneNumber);
        EditTextUtils.trimEditText(emergencyMessage);
        EditTextUtils.trimEditText(email);
        EditTextUtils.trimEditText(firstName);
        EditTextUtils.trimEditText(lastName);
    }
}

