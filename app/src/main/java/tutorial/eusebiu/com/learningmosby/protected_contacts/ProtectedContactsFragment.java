package tutorial.eusebiu.com.learningmosby.protected_contacts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby.mvp.MvpFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import tutorial.eusebiu.com.learningmosby.R;
import tutorial.eusebiu.com.learningmosby.dagger.DaggerMainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainModule;
import tutorial.eusebiu.com.learningmosby.main.ContactsAdapter;
import tutorial.eusebiu.com.learningmosby.models.Contact;
import tutorial.eusebiu.com.learningmosby.models.User;
import tutorial.eusebiu.com.learningmosby.navigation.AcceptProtectedEvent;
import tutorial.eusebiu.com.learningmosby.navigation.DeleteProtectedEvent;
import tutorial.eusebiu.com.learningmosby.utils.ComplexPreferences;
import tutorial.eusebiu.com.learningmosby.utils.LoadingDialog;
import tutorial.eusebiu.com.learningmosby.utils.PhoneUtils;
import tutorial.eusebiu.com.learningmosby.utils.ShortMessageManager;

import static android.R.id.list;

/**
 * Created by Sebi on 3/27/2017.
 */

public class ProtectedContactsFragment extends MvpFragment<ProtectedContactsView, ProtectedContactsPresenter> implements ProtectedContactsView {

    MainComponent component;
    View view;

    @Inject
    EventBus eventBus;

    @Inject
    ComplexPreferences complexPreferences;

    @Inject
    ShortMessageManager toastMaker;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    ContactsAdapter adapter;

    @Inject
    LoadingDialog loadingDialog;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_protected, null);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initBaseControls();
        if(!eventBus.isRegistered(this))
            eventBus.register(this);
        setFields();
        User user = complexPreferences.getObject("user", User.class);
        presenter.tryGetProtected(user.getPhoneNumber());


    }

    @Subscribe
    public void onEvent(AcceptProtectedEvent event) {
        presenter.tryAcceptProtected(complexPreferences.getObject("user", User.class).getPhoneNumber(), event.getContactPhoneNumber());

    }

    @Subscribe
    public void onEvent(DeleteProtectedEvent event) {
        presenter.tryDeleteProtected(complexPreferences.getObject("user", User.class).getPhoneNumber(), event.getContactPhoneNumber());
    }

    private void setFields() {
        adapter = getComponent().contactsAdapter();
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);
    }

    protected MainComponent getComponent() {
        if (component == null) {
            component = DaggerMainComponent.builder().mainModule(new MainModule(this.getContext())).build();
        }
        return component;
    }

    void initBaseControls() {
        ButterKnife.bind(this, view);
        getComponent().inject(this);
    }

    @Override
    public ProtectedContactsPresenter createPresenter() {
        return getComponent().protectedPresenter();
    }

    @Override
    public void showLoading(boolean show) {

        if(show)
            loadingDialog.show();
        else
            loadingDialog.hide();
    }

    @Override
    public void setAdapterData(List<Contact> list) {

        adapter.setmDataset(list);
    }

    @Override
    public void showToast(String toast) {
        toastMaker.showShortMessage(toast);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(eventBus.isRegistered(this))
            eventBus.unregister(this);
    }
}
