package tutorial.eusebiu.com.learningmosby.edit_profile;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Created by Sebi on 4/22/2017.
 */

public interface EditProfileView extends MvpView {

    void showLoading(boolean show);
    void showToast(String toast);
    void setData();
}
