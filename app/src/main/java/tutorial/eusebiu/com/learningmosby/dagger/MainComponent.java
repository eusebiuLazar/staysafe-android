package tutorial.eusebiu.com.learningmosby.dagger;

import javax.inject.Singleton;

import dagger.Component;
import tutorial.eusebiu.com.learningmosby.edit_profile.EditProfileActivity;
import tutorial.eusebiu.com.learningmosby.edit_profile.EditProfilePresenter;
import tutorial.eusebiu.com.learningmosby.fcm.MyFirebaseInstanceIDService;
import tutorial.eusebiu.com.learningmosby.forgot_password.ForgotPasswordActivity;
import tutorial.eusebiu.com.learningmosby.forgot_password.ForgotPasswordPresenter;
import tutorial.eusebiu.com.learningmosby.invite.InviteFragment;
import tutorial.eusebiu.com.learningmosby.invite.InvitePresenter;
import tutorial.eusebiu.com.learningmosby.login.LoginActivity;
import tutorial.eusebiu.com.learningmosby.login.LoginPresenter;
import tutorial.eusebiu.com.learningmosby.main.ContactsAdapter;
import tutorial.eusebiu.com.learningmosby.main.MainActivity;
import tutorial.eusebiu.com.learningmosby.main.MainPresenter;
import tutorial.eusebiu.com.learningmosby.protectors.AddProtectorsAdapter;
import tutorial.eusebiu.com.learningmosby.protected_contacts.ProtectedContactsFragment;
import tutorial.eusebiu.com.learningmosby.protected_contacts.ProtectedContactsPresenter;
import tutorial.eusebiu.com.learningmosby.protectors.ProtectorsFragment;
import tutorial.eusebiu.com.learningmosby.protectors.ProtectorsPresenter;
import tutorial.eusebiu.com.learningmosby.receivers.LockReceiver;
import tutorial.eusebiu.com.learningmosby.register.RegisterActivity;
import tutorial.eusebiu.com.learningmosby.register.RegisterPresenter;
import tutorial.eusebiu.com.learningmosby.splash.SplashActivity;

/**
 * Created by mihai on 05/12/16.
 */
@Component(
        modules = {
                MainModule.class
        }
)
@Singleton
public interface MainComponent {
    void inject(MainActivity activity);

    void inject(LoginActivity activity);

    LoginPresenter loginPresenter();

    RegisterPresenter registerPresenter();

    MainPresenter presenter();

    ProtectorsPresenter protectorsPresenter();

    ProtectedContactsPresenter protectedPresenter();

    InvitePresenter invitePresenter();

    ForgotPasswordPresenter forgotPasswordPresenter();

    ContactsAdapter contactsAdapter();

    AddProtectorsAdapter addProtectorsAdapter();

    EditProfilePresenter editProfilePresenter();

    void inject (RegisterActivity registerActivity);

    void inject(ProtectorsFragment protectorsFragment);

    void inject(ProtectedContactsFragment protectedContactsFragment);

    void inject(InviteFragment inviteFragment);

    void inject(ForgotPasswordActivity activity);

    void inject(SplashActivity activity);

    void inject(LockReceiver receiver);

    void inject(EditProfileActivity activity);

    void inject(MyFirebaseInstanceIDService service);

}
