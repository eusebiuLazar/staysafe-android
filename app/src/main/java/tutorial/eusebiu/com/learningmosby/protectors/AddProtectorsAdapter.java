package tutorial.eusebiu.com.learningmosby.protectors;

import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import tutorial.eusebiu.com.learningmosby.R;
import tutorial.eusebiu.com.learningmosby.models.Contact;
import tutorial.eusebiu.com.learningmosby.navigation.RefreshProtectorsEvent;

/**
 * Created by Sebi on 4/10/2017.
 */

public class AddProtectorsAdapter extends RecyclerView.Adapter<AddProtectorsAdapter.AddProtectedViewHolder>{

    @Inject
    EventBus eventBus;

    @Inject
    public AddProtectorsAdapter() {

    }

    private Set<Contact> selectedContacts = new HashSet<>();

    public List<Contact> getSelectedContacts() {
        List<Contact> selContacts = new ArrayList<>();

        for (Contact contact : selectedContacts) {
            selContacts.add(contact);
        }

        return selContacts;
    }

    public void setDataset(List<Contact> mDataset) {
        this.mDataset = mDataset;
        selectedContacts.clear();
        notifyDataSetChanged();
    }

    private List<Contact> mDataset = new ArrayList<>();



    // Create new views (invoked by the layout manager)
    @Override
    public AddProtectedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.add_protectors_cell, parent, false);

        AddProtectedViewHolder vh = new AddProtectedViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final AddProtectedViewHolder holder, int position) {

        Contact contact = mDataset.get(position);
        holder.phoneNumber.setText(contact.getPhoneNumber());
        holder.name.setText(contact.getName());

        holder.checkBox.setChecked(selectedContacts.contains(mDataset.get(holder.getAdapterPosition())));

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    selectedContacts.add(mDataset.get(holder.getAdapterPosition()));
                } else {
                    selectedContacts.remove(mDataset.get(holder.getAdapterPosition()));
                }
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    static class AddProtectedViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView name, phoneNumber;
        CheckBox checkBox;

        public AddProtectedViewHolder(View view) {

            super(view);
            imageView =(ImageView) view.findViewById(R.id.avatar);
            name = (TextView) view.findViewById(R.id.name);
            phoneNumber = (TextView) view.findViewById(R.id.phone_number);
            checkBox = (CheckBox) view.findViewById(R.id.checkbox);
        }



    }

}
