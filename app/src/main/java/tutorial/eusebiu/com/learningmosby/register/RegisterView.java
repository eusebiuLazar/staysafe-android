package tutorial.eusebiu.com.learningmosby.register;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Created by Sebi on 3/25/2017.
 */

public interface RegisterView extends MvpView {

    void showToast(String toast);

    void onRegisterSuccess();

    void showLoading(boolean show);
}
