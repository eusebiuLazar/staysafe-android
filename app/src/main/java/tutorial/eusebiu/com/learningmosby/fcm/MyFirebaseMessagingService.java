package tutorial.eusebiu.com.learningmosby.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import tutorial.eusebiu.com.learningmosby.R;
import tutorial.eusebiu.com.learningmosby.main.MainActivity;
import tutorial.eusebiu.com.learningmosby.map.MapActivity;

import static android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP;

/**
 * Created by Sebi on 5/25/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage.getData() != null)
            sendNotification(remoteMessage);
    }

    private void sendNotification(RemoteMessage message) {
        String messageBody = message.getData().get("message");
        String messageTitle = message.getData().get("title");

        Intent intent = new Intent(this, MapActivity.class);
        intent.putExtras(makeBundleFromData(message));
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT
                        | PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(NotificationCompat.DEFAULT_SOUND | NotificationCompat.DEFAULT_VIBRATE)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    private Bundle makeBundleFromData(RemoteMessage remoteMessage) {
        Bundle bundle = new Bundle();
        bundle.putDouble("lat", Double.valueOf(remoteMessage.getData().get("lat")));
        bundle.putDouble("lng", Double.valueOf(remoteMessage.getData().get("lng")));
        bundle.putString("address", remoteMessage.getData().get("address"));
        bundle.putLong("timestamp", Long.valueOf(remoteMessage.getData().get("timestamp")));
        bundle.putString("message", remoteMessage.getData().get("message"));
        return bundle;
    }
}