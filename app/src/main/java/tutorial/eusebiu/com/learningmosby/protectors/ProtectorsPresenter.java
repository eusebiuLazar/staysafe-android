package tutorial.eusebiu.com.learningmosby.protectors;

import android.os.AsyncTask;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import tutorial.eusebiu.com.learningmosby.api.Api;
import tutorial.eusebiu.com.learningmosby.api.requests.GetProtectorsRequest;
import tutorial.eusebiu.com.learningmosby.api.requests.RelationshipRequest;
import tutorial.eusebiu.com.learningmosby.api.requests.SendRequestToProtectorsRequest;
import tutorial.eusebiu.com.learningmosby.api.responses.BaseResponse;
import tutorial.eusebiu.com.learningmosby.api.responses.GetProtectorsResponse;
import tutorial.eusebiu.com.learningmosby.models.Contact;
import tutorial.eusebiu.com.learningmosby.models.EmergencyList;
import tutorial.eusebiu.com.learningmosby.utils.ComplexPreferences;

/**
 * Created by Sebi on 3/27/2017.
 */

public class ProtectorsPresenter extends MvpBasePresenter<ProtectorsView> {

    @Inject
    Api api;

    @Inject
    ComplexPreferences complexPreferences;

    @Inject
    public ProtectorsPresenter() {
    }

    public void tryGetProtectors(String phoneNumber) {

        api.getProtectors(new GetProtectorsRequest(phoneNumber))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GetProtectorsResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(isViewAttached()) {
                            getView().showLoading(false);
                            getView().showToast("Conexiune nereusita!");
                        }

                    }

                    @Override
                    public void onNext(GetProtectorsResponse getProtectorsResponse) {
                        if (isViewAttached())
                            getView().showLoading(false);

                        if(!getProtectorsResponse.isSuccess())
                        {
                            if(isViewAttached())
                                getView().showToast(getProtectorsResponse.getMessage());
                        }
                        else
                        {
                            if(isViewAttached()) {

                                List<Contact> contacts = getProtectorsResponse.getContacts();

                                Set<String> contactSet = new HashSet<>();
                                for (Contact contact : contacts) {
                                    if (contact.getStatus().equals(Contact.STATUS_ACCEPTED))
                                        contactSet.add(contact.getPhoneNumber());
                                }

                                EmergencyList emergencyList = new EmergencyList(contactSet);
                                complexPreferences.putObject("emergency_list", emergencyList);

                                getView().setAdapterData(contacts);
                            }
                        }
                    }
                });

    }

    AsyncTask<Void, Void, List<Contact>> dbAsync = null;

    public void getContactsFromLocalDB(final String filter) {

        if (dbAsync != null)
            dbAsync.cancel(true);

        dbAsync = new AsyncTask<Void, Void, List<Contact>>() {

            @Override
            protected List<Contact> doInBackground(Void... voids) {
                if (isCancelled())
                    return new ArrayList<>();

                return Contact.findWithQuery(Contact.class, "SELECT * FROM Contact WHERE phone_number LIKE '%" + filter + "%' OR name LIKE '%" + filter + "%'");
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                if (isCancelled())
                    return;

                if(isViewAttached())
                    getView().showLoading(true);

            }

            @Override
            protected void onPostExecute(List<Contact> contacts) {
                super.onPostExecute(contacts);

                if (isCancelled())
                    return;

                if(isViewAttached()) {
                    getView().setDialogAdapterData(contacts);
                    getView().showLoading(false);
                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void tryDeleteProtector(final String userPhoneNumber, String contactPhoneNumber) {

        api.deleteProtector(new RelationshipRequest(userPhoneNumber, contactPhoneNumber))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(isViewAttached()) {
                            getView().showLoading(false);
                            getView().showToast("Conexiune nereusita!");
                        }

                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (isViewAttached())
                            getView().showLoading(false);

                        if(!baseResponse.isSuccess())
                        {
                            if(isViewAttached())
                                getView().showToast(baseResponse.getMessage());
                        }
                        else
                        {
                           tryGetProtectors(userPhoneNumber);
                        }
                    }
                });

    }

    public void trySendRequestToProtectors(final String userPhoneNumber, List<Contact> protectors) {

        api.sendRequestToProtectors(new SendRequestToProtectorsRequest(userPhoneNumber, protectors))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(isViewAttached()) {
                            getView().showLoading(false);
                            getView().showToast("Conexiune nereusita!");
                        }

                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (isViewAttached())
                            getView().showLoading(false);

                        tryGetProtectors(userPhoneNumber);
                    }
                });

    }
}
