package tutorial.eusebiu.com.learningmosby.login;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Created by Sebi on 3/19/2017.
 */

public interface LoginView extends MvpView {

    void onLoginSuccess();

    void OnError();

    void showLoading(boolean show);

    void showToast(String toast);
}
