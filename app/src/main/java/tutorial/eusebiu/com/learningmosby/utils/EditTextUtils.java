package tutorial.eusebiu.com.learningmosby.utils;

import android.widget.EditText;

/**
 * Created by Sebi on 4/23/2017.
 */

public class EditTextUtils {
    public static void trimEditText(EditText e) {
        e.setText(e.getText().toString().trim());
    }
}
