package tutorial.eusebiu.com.learningmosby.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import java.io.File;

public class ImagePicker {

    private static final int DEFAULT_WIDTH = 500;
    private static final int DEFAULT_HEIGHT = 500;

    private int targetW;
    private int targetH;

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_IMAGE_PIC = 2;

    private String imagePath;

    private Activity activity;
    private Fragment fragment;

    public ImagePicker(Activity activity)
    {
        this.activity = activity;

        targetW = DEFAULT_WIDTH;
        targetH = DEFAULT_HEIGHT;
    }

    public ImagePicker(Fragment fragment)
    {
        this.fragment = fragment;

        targetW = DEFAULT_WIDTH;
        targetH = DEFAULT_HEIGHT;
    }

    public void setTargetImageSize(int width, int height)
    {
        this.targetW = width;
        this.targetH = height;
    }

    public void showImagePickDialog()
    {
        final CharSequence[] items = { "Take Photo", "Choose from Library", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getCommonContext());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    dispatchTakePictureIntent();
                } else if (items[item].equals("Choose from Library")) {
                    dispatchPickPictureIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void dispatchPickPictureIntent()
    {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        if (activity != null) {
            activity.startActivityForResult(
                    Intent.createChooser(intent, "Select File"),
                    REQUEST_IMAGE_PIC);
        }
        else
        {
            fragment.startActivityForResult(
                    Intent.createChooser(intent, "Select File"),
                    REQUEST_IMAGE_PIC);
        }
    }

    private Context getCommonContext()
    {
        return activity == null ? fragment.getActivity() : activity;
    }

    private void dispatchTakePictureIntent() {
        String dirPath = Environment
                .getExternalStorageDirectory()
                + File.separator + "Aroundwise" + File.separator;

        File root = new File(dirPath);
        root.mkdirs();

        imagePath = dirPath + "profilepic.jpg";

        File sdImageMainDirectory = new File(root, "profilepic.jpg");
        sdImageMainDirectory.delete();

        Uri imageCaptureUri = Uri.fromFile(sdImageMainDirectory);

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (activity != null) {
            if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageCaptureUri);
                activity.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
        else
        {
            if (takePictureIntent.resolveActivity(fragment.getActivity().getPackageManager()) != null)
            {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageCaptureUri);
                fragment.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    public Bitmap getPictureFromGallery(Intent data)
    {
        Uri selectedImage = data.getData();
        String[] filePathColumn = { MediaStore.Images.Media.DATA };

        // Get the cursor
        Cursor cursor = getCommonContext().getContentResolver().query(selectedImage,
                filePathColumn, null, null, null);
        // Move to first row

        //TODO: treat error
        if (cursor == null)
        {
            Toast.makeText(getCommonContext(), "Could not select photo!", Toast.LENGTH_SHORT);
            return null;
        }

        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        imagePath = cursor.getString(columnIndex);
        cursor.close();
        if (imagePath == null)
        {
            Toast.makeText(getCommonContext(), "Could not select photo!", Toast.LENGTH_SHORT);
            return null;
        }

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        try {
            BitmapFactory.decodeFile(imagePath, bmOptions);
        }
        catch (Exception e)
        {
            Log.d("TEST", "Bitmap exception: " + e.getLocalizedMessage());
        }
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, bmOptions);

        try {
            ExifInterface ei = new ExifInterface(imagePath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            Bitmap rotatedBitmap = bitmap;

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateBitmap(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateBitmap(bitmap, 180);
                    break;
                // etc.
            }

            return rotatedBitmap;


        } catch (Exception e) {
            Toast.makeText(getCommonContext(), "Could not select photo!", Toast.LENGTH_SHORT).show();
            return null;
        }
    }

    public Bitmap getPictureFromCamera()
    {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        try {
            BitmapFactory.decodeFile(imagePath, bmOptions);
        }
        catch (Exception e)
        {
            Log.d("TEST", "Bitmap exception: " + e.getLocalizedMessage());
        }
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, bmOptions);

        try {
            ExifInterface ei = new ExifInterface(imagePath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            Bitmap rotatedBitmap = bitmap;

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateBitmap(bitmap, 270);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateBitmap(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateBitmap(bitmap, 180);
                    break;
                // etc.
            }

            File file = new File(imagePath);
            file.delete();

            return rotatedBitmap;


        } catch (Exception e) {
            Toast.makeText(getCommonContext(), "Could not select photo!", Toast.LENGTH_SHORT).show();
            return null;
        }
    }

    private Bitmap rotateBitmap(Bitmap source, int rotation) {
        Matrix matrix = new Matrix();
        matrix.setRotate(rotation, source.getWidth() / 2, source.getHeight() / 2);

        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
}
