package tutorial.eusebiu.com.learningmosby.main;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

import tutorial.eusebiu.com.learningmosby.api.Api;
import tutorial.eusebiu.com.learningmosby.models.Contact;
import tutorial.eusebiu.com.learningmosby.navigation.ContactsInsertedEvent;
import tutorial.eusebiu.com.learningmosby.utils.ComplexPreferences;
import tutorial.eusebiu.com.learningmosby.utils.PhoneUtils;

/**
 * Created by mihai on 05/12/16.
 */

public class MainPresenter extends MvpBasePresenter<MainView> {

    @Inject
    Api api;

    @Inject
    EventBus eventBus;

    @Inject
    public MainPresenter() {

    }

    @Inject
    ComplexPreferences complexPreferences;

    @Inject
    Context context;

    public void loadContacts(final ContentResolver contentResolver) {

        final SharedPreferences prefs = context.getSharedPreferences(
                "tutorial.eusebiu.com.learningmosby", Context.MODE_PRIVATE);

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                PhoneUtils.getContactsFromPhone(contentResolver);
                return null;
            }

            @Override
            protected void onPostExecute(Void v) {
                super.onPostExecute(v);
                boolean isFirstTime = prefs.getBoolean("isFirstTime", true);
                if(isFirstTime) {
                    prefs.edit().putBoolean("isFirstTime", false).commit();

                }
                if(isViewAttached())
                    getView().showLoading(false);

                eventBus.post(new ContactsInsertedEvent());

            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                boolean isFirstTime = prefs.getBoolean("isFirstTime", true);
                if(isViewAttached() && isFirstTime == true)
                    getView().showLoading(true);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

}
