package tutorial.eusebiu.com.learningmosby.fcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import tutorial.eusebiu.com.learningmosby.api.Api;
import tutorial.eusebiu.com.learningmosby.api.RestClient;
import tutorial.eusebiu.com.learningmosby.api.requests.UpdateFcmTokenRequest;
import tutorial.eusebiu.com.learningmosby.api.responses.BaseResponse;
import tutorial.eusebiu.com.learningmosby.dagger.DaggerMainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainModule;
import tutorial.eusebiu.com.learningmosby.models.User;
import tutorial.eusebiu.com.learningmosby.utils.ComplexPreferences;

/**
 * Created by Sebi on 5/25/2017.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Inject
    ComplexPreferences complexPreferences;

    @Inject
    Api api;

    MainComponent component;

    @Override
    public void onTokenRefresh() {

        getComponent().inject(this);

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        complexPreferences.putObject("fcmToken", refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }


    private void sendRegistrationToServer(final String token) {
        User loggedInUser = complexPreferences.getObject("user", User.class);

        if (!isUserLoggedIn(loggedInUser)) {
            return;
        }

        api.updateFcmToken(new UpdateFcmTokenRequest(loggedInUser.getPhoneNumber(), loggedInUser.getFcmToken()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                sendRegistrationToServer(token);
                            }
                        }, 5000);
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {

                    }
                });
    }

    protected MainComponent getComponent() {
        if (component == null) {
            component = DaggerMainComponent.builder().mainModule(new MainModule(this)).build();
        }
        return component;
    }

    public boolean isUserLoggedIn(User loggedInUser) {

        if (loggedInUser == null)
            return false;

        if (loggedInUser.getPhoneNumber() == null)
            return false;

        return true;
    }
}
