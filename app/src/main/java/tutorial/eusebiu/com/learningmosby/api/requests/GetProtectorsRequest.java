package tutorial.eusebiu.com.learningmosby.api.requests;

/**
 * Created by Sebi on 4/9/2017.
 */

public class GetProtectorsRequest {
    private String phoneNumber;

    public GetProtectorsRequest(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
