package tutorial.eusebiu.com.learningmosby.utils;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.List;

import tutorial.eusebiu.com.learningmosby.main.ContactsAdapter;
import tutorial.eusebiu.com.learningmosby.models.Contact;

/**
 * Created by Sebi on 4/8/2017.
 */

public class PhoneUtils {

    public static void getContactsFromPhone(ContentResolver contentResolver) {


        ContentResolver cr = contentResolver;
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        while (cursor.moveToNext()) {
            String contactId =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));

            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            //
            //  Get all phone numbers.
            //
            String number = "";
            Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
            while (phones.moveToNext()) {
                number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                break;
            }
            phones.close();

            if (!name.equals("") && !number.equals("")) {
                number = number.replaceAll(" ", "");
                number = number.replaceAll("-", "");

                if (number.length() == 12)
                    number = number.substring(2);

                if (number.length() == 10) {
                    Contact contact = new Contact(name, number);
                    contact.saveWithoutStatus();
                }
            }

        }
        cursor.close();

    }
}
