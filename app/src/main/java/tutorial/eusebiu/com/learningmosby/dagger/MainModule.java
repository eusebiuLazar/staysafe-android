package tutorial.eusebiu.com.learningmosby.dagger;

import android.content.Context;
import android.view.LayoutInflater;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import tutorial.eusebiu.com.learningmosby.api.Api;
import tutorial.eusebiu.com.learningmosby.api.RestClient;
import tutorial.eusebiu.com.learningmosby.utils.ComplexPreferences;
import tutorial.eusebiu.com.learningmosby.utils.LoadingDialog;
import tutorial.eusebiu.com.learningmosby.utils.ShortMessageManager;

/**
 * Created by mihai on 05/12/16.
 */

@Module()
public class MainModule {
    private Context context;

    public MainModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    public Api getMoviesApi() {
        return RestClient.getInstance();
    }

    @Provides
    @Singleton
    public RequestManager providesGlide()
    {
        return Glide.with(context);
    }

    @Provides
    @Singleton
    public LayoutInflater providesLayoutInflater() {
        return ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Provides
    @Singleton
    public ComplexPreferences providesComplexPreferences() {
        return ComplexPreferences.getComplexPreferences(context, null, Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    public ShortMessageManager providesShortMessageManager() {
        return new ShortMessageManager(context);
    }

    @Provides
    @Singleton
    public LoadingDialog providesLoadingDialog() {
        return new LoadingDialog(context);
    }

    @Provides
    @Singleton
    public EventBus providesEventBus() { return EventBus.getDefault(); }

    @Provides
    @Singleton
    public Context providesContext() { return context; }
}
