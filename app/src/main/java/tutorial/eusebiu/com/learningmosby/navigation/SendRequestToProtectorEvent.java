package tutorial.eusebiu.com.learningmosby.navigation;

/**
 * Created by Sebi on 4/10/2017.
 */

public class SendRequestToProtectorEvent {

    String contactPhoneNumber;

    public String getContactPhoneNumber() {
        return contactPhoneNumber;
    }

    public void setContactPhoneNumber(String contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
    }

    public SendRequestToProtectorEvent(String contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
    }
}
