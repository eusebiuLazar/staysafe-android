package tutorial.eusebiu.com.learningmosby.api.responses;

import java.util.List;

import tutorial.eusebiu.com.learningmosby.models.Contact;
import tutorial.eusebiu.com.learningmosby.models.User;

/**
 * Created by Sebi on 4/9/2017.
 */

public class GetProtectorsResponse extends BaseResponse {
    private List<Contact> contacts;

    public GetProtectorsResponse(List<Contact> contacts, String message, boolean success) {
        super(message, success);
        this.contacts = contacts;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }
}
