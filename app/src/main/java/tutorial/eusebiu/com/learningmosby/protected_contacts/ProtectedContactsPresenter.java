package tutorial.eusebiu.com.learningmosby.protected_contacts;

import android.content.ContentResolver;
import android.os.AsyncTask;
import android.widget.TextView;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import tutorial.eusebiu.com.learningmosby.R;
import tutorial.eusebiu.com.learningmosby.api.Api;
import tutorial.eusebiu.com.learningmosby.api.requests.GetProtectorsRequest;
import tutorial.eusebiu.com.learningmosby.api.requests.RelationshipRequest;
import tutorial.eusebiu.com.learningmosby.api.responses.BaseResponse;
import tutorial.eusebiu.com.learningmosby.api.responses.GetProtectorsResponse;
import tutorial.eusebiu.com.learningmosby.models.Contact;
import tutorial.eusebiu.com.learningmosby.models.User;
import tutorial.eusebiu.com.learningmosby.protectors.ProtectorsView;
import tutorial.eusebiu.com.learningmosby.utils.PhoneUtils;

/**
 * Created by Sebi on 3/27/2017.
 */

public class ProtectedContactsPresenter extends MvpBasePresenter<ProtectedContactsView> {

    @Inject
    Api api;

    @Inject
    public ProtectedContactsPresenter() {
    }

    public void tryGetProtected(String phoneNumber) {

        api.getProtected(new GetProtectorsRequest(phoneNumber))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GetProtectorsResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(isViewAttached()) {
                            getView().showLoading(false);
                            getView().showToast("Conexiune nereusita!");
                        }

                    }

                    @Override
                    public void onNext(GetProtectorsResponse getProtectorsResponse) {
                        if (isViewAttached())
                            getView().showLoading(false);

                        if(!getProtectorsResponse.isSuccess())
                        {
                            if(isViewAttached())
                                getView().showToast(getProtectorsResponse.getMessage());
                        }
                        else
                        {
                            if(isViewAttached())
                                getView().setAdapterData(getProtectorsResponse.getContacts());
                        }
                    }
                });

    }

    public void tryDeleteProtected(final String userPhoneNumber, String contactPhoneNumber) {

        api.deleteProtected(new RelationshipRequest(userPhoneNumber, contactPhoneNumber))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(isViewAttached()) {
                            getView().showLoading(false);
                            getView().showToast("Conexiune nereusita!");
                        }

                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (isViewAttached())
                            getView().showLoading(false);

                        if(!baseResponse.isSuccess())
                        {
                            if(isViewAttached())
                                getView().showToast(baseResponse.getMessage());
                        }
                        else
                        {
                            tryGetProtected(userPhoneNumber);
                        }
                    }
                });

    }

    public void tryAcceptProtected(final String userPhoneNumber, String contactPhoneNumber) {

        api.confirmProtected(new RelationshipRequest(userPhoneNumber, contactPhoneNumber))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(isViewAttached()) {
                            getView().showLoading(false);
                            getView().showToast("Conexiune nereusita!");
                        }

                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (isViewAttached())
                            getView().showLoading(false);

                        if(!baseResponse.isSuccess())
                        {
                            if(isViewAttached())
                                getView().showToast(baseResponse.getMessage());
                        }
                        else
                        {
                             tryGetProtected(userPhoneNumber);
                        }
                    }
                });

    }



    public void loadContacts(final ContentResolver contentResolver) {

     /*   new AsyncTask<Void, Void, List<User>>() {

            @Override
            protected List<User> doInBackground(Void... voids) {
             //   GetProtectorsResponse getProtectorsResponse = new GetProtectorsResponse();

            }

            @Override
            protected void onPostExecute(List<Contact> list) {
                super.onPostExecute(list);
                if(isViewAttached()) {
                    getView().showLoading(false);
                    getView().setAdapterData(list);
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if(isViewAttached())
                    getView().showLoading(true);
            }
        }.execute();*/
    }
}
