package tutorial.eusebiu.com.learningmosby.models;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.util.List;

/**
 * Created by Sebi on 4/8/2017.
 */

public class Contact extends SugarRecord {

    public static final String STATUS_PENDING = "pending";
    public static final String STATUS_USER = "user";
    public static final String STATUS_ANONYMOUS = "anonymous";
    public static final String STATUS_ACCEPTED = "accepted";
    public static final String STATUS_REQUEST = "request";

    private String name="";

    @Unique
    private String phoneNumber="";

    private String status="";


    public Contact(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber.replaceAll(" ", "");
    }

    public Contact() {

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber.replaceAll(" ", "");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public long save() {
        List<Contact> list = find(Contact.class, "phone_number = ?", phoneNumber);
        if (list.size() > 0) {
            this.setId(list.get(0).getId());
        }

        return super.save();
    }

    public long saveWithoutStatus() {
        List<Contact> list = find(Contact.class, "phone_number = ?", phoneNumber);
        if (list.size() > 0) {
            this.setId(list.get(0).getId());
            this.setStatus(list.get(0).getStatus());
        }

        return super.save();
    }

    @Override
    public boolean equals(Object obj) {
        Contact ob = (Contact) obj;
        return (phoneNumber.equals(ob.phoneNumber) && name.equals(ob.name) && status.equals(ob.status));
    }
}
