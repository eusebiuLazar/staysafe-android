package tutorial.eusebiu.com.learningmosby.receivers;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import tutorial.eusebiu.com.learningmosby.api.Api;
import tutorial.eusebiu.com.learningmosby.api.requests.SendEmergencyMessageRequest;
import tutorial.eusebiu.com.learningmosby.api.responses.BaseResponse;
import tutorial.eusebiu.com.learningmosby.dagger.DaggerMainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainComponent;
import tutorial.eusebiu.com.learningmosby.dagger.MainModule;
import tutorial.eusebiu.com.learningmosby.models.EmergencyList;
import tutorial.eusebiu.com.learningmosby.models.User;
import tutorial.eusebiu.com.learningmosby.utils.ComplexPreferences;
import tutorial.eusebiu.com.learningmosby.utils.DateUtils;

/**
 * Created by Sebi on 4/17/2017.
 */

public class LockReceiver extends BroadcastReceiver implements LocationListener {

    MainComponent component;

    private int numberOfPresses = 0;

    private long lastTime = 0;

    @Inject
    ComplexPreferences complexPreferences;

    @Inject
    Api api;

    private Context context;

    private User loggedInUser;

    private boolean foundFirstLocation = false;

    public LockReceiver() {

    }

    public LockReceiver(Context context) {
        this.context = context;
        getComponent().inject(this);
        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    private void getEmergencyLocation() {
        foundFirstLocation = false;

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Getting the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);
        locationManager.requestLocationUpdates(provider, 500, 0, this);

        Location location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

        if (location == null)
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);   // try everything

        if (location == null)
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);   //really try everything

        if (location == null)
            location = complexPreferences.getObject("lastKnownLocation", Location.class);

        if (location != null) {
            onLocationChanged(location);
        }
    }

    protected MainComponent getComponent() {
        if (component == null) {
            component = DaggerMainComponent.builder().mainModule(new MainModule(context)).build();
        }
        return component;
    }

    @Override
    public void onReceive(Context arg0, Intent arg1) {

        loggedInUser = complexPreferences.getObject("user", User.class);
        if (!isUserLoggedIn()) {
            return;
        }

        long currentTime = System.currentTimeMillis();

        if (numberOfPresses == 0) {
            numberOfPresses++;
            lastTime = currentTime;
        } else {

            if (currentTime - lastTime > 1000) {
                numberOfPresses = 1;
                lastTime = currentTime;
            }
            else {
                numberOfPresses++;
                lastTime = currentTime;
            }

        }

        if (numberOfPresses == 3) {
            numberOfPresses = 0;
            getEmergencyLocation();
        }
    }

    @Override
    public void onLocationChanged(final Location location) {
        if (!foundFirstLocation) {
            foundFirstLocation = true;

            new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... voids) {
                    try {
                        return getAddressFromLatLng(location);
                    } catch (IOException e) {
                        return "";
                    }
                }

                @Override
                protected void onPostExecute(String address) {
                    super.onPostExecute(address);

                    long currentTime = System.currentTimeMillis();

                    if (isUserLoggedIn() && !address.equals("")) {
                        api.sendEmergencyMessage(new SendEmergencyMessageRequest(loggedInUser.getPhoneNumber(), location.getLatitude(), location.getLongitude(), address, currentTime))
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Observer<BaseResponse>() {
                                    @Override
                                    public void onCompleted() {

                                    }

                                    @Override
                                    public void onError(Throwable e) {

                                    }

                                    @Override
                                    public void onNext(BaseResponse baseResponse) {

                                    }
                                });

                        String smsMessage = loggedInUser.getFirstName() + " " + loggedInUser.getLastName() + " are nevoie de ajutorul tău! Mesajul său: " + loggedInUser.getEmergencyMessage() + "\n\nMesajul a fost trimis la " + DateUtils.formatDate(currentTime)
                                + " de la " + address + " (lat: " + location.getLatitude() + ", lng: " + location.getLongitude() +")";
                        sendEmergencyTexts(smsMessage);
                    }

                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private void sendEmergencyTexts(String msg) {
        EmergencyList emergencyList = complexPreferences.getObject("emergency_list", EmergencyList.class);

        if (emergencyList != null && emergencyList.getPhones().size() > 0)
            for (String phone:emergencyList.getPhones()) {
                sendSMS(phone, msg);
            }
    }

    private void sendSMS(String phoneNo, String msg) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
          /*  Toast.makeText(getApplicationContext(), "Message Sent",
                    Toast.LENGTH_LONG).show();*/
        } catch (Exception ex) {
          /*  Toast.makeText(getApplicationContext(),ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();*/
            ex.printStackTrace();
        }
    }

    private String getAddressFromLatLng(Location location) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());

        addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();

        if (city == null || city.equals("null"))
            city = "";
        if (address == null || address.equals("null"))
            address = "";

        return address + " " + city;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private boolean isUserLoggedIn() {
        if (loggedInUser == null)
            return false;

        if (loggedInUser.getPhoneNumber() == null)
            return false;

        return true;
    }
}
